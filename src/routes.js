const routes = require('express').Router();

const UserController = require('./app/controllers/UserController');
const BlogController = require('./app/controllers/BlogController');

routes.get('/', (req, res) => {
  res.sendFile(__dirname + '/html/index.html');
});

/*--CRUD for USERS--*/

routes.post('/login', UserController.store);
routes.post('/users', UserController.create);
routes.get('/users', UserController.readAll);

routes.get('/users/:id', UserController.read);
routes.put('/users/:id', UserController.update);
routes.delete('/users/:id', UserController.delete);

/*--CRUD for BLOG--*/

routes.get('/blog', BlogController.readAll);
routes.post('/blog', BlogController.create);

module.exports = routes;