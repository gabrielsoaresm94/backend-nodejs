module.exports = (sequelize, DataTypes) => {
    const Post = sequelize.define(
    'Post',
    {
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        title: DataTypes.STRING,
        content: DataTypes.STRING
    });

    return Post;
}