const { User } = require('../models');

class UserController {
    async store(req, res) {
        const { email, password} = req.body;

        const user = await User.findOne({ where: { email } });

        if (!user) {
            return res.status(401).json({ message: "User not found" });
        }

        if (!(await user.checkPassword(password))) {
            return res.status(401).json({ message: "Incorrect password" });
      }

      return res.json({
          user,
          token: user.generateToken()
      });

      //return res.status(200).send();
    }

    /*async create(req, res, next) {
        //return User.create(req.body)
        //.then((user) => res.status(200).send(user))
        //.catch((error) => res.status(400).send(error))
        const user = await User.create(req.body);
        res.status(200).json(user).send(user);
    }*/

    async create(req, res) {
        /*const { name, email, password} = req.password;

        const user = await User.create({
            name: name, 
            email: email, 
            password: password
        });*/
        
        res.status(200).json(user).send(user);
    }

    async read(req, res, next) {
        let id = req.params.id;
        const user = await User.findOne({ where: {id} });
        if (!user) {
            return res.status(401).json({ message: "User not found" });
        }
        res.status(201).json(user)
    }

    async readAll(req, res, next) {
        let id = req.params.id;
        const user = await User.findAll(req.body);
        res.json(user).send('All in register');
    }

    async update(req, res, next) {
        res.json(`Im in update ${id}`);
    }

    async delete(req, res, next) {
        res.json(`Im in delete ${id}`);
    }

}

module.exports = new UserController;