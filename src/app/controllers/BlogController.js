const { Post } = require('../models');

class BlogController {

    async create(req, res, next) {
        const post = await Post.create(req.body);
        res.status(200).json(post).send(post);
    }

    async readAll(req, res, next) {
        const post = await Post.findAll(req.body);
        res.json(post);
    }
}

module.exports = new BlogController;