const request = require('supertest');

const app = require('../../src/index');
const factory = require('../factories');
const truncate = require('../utils/truncate');

describe('Authentication', () => {
    beforeEach(async () => {
        await truncate();
    });

    it('should authenticate with valid credentials', async () => {
        const user = await factory.create('User', {
            password: '123456'
        });

        const response = await request(app)
            .post('/login')
            .send({
                email: user.email,
                password: '123456'
            })

        expect(response.status).toBe(200);
    });

    it('should authenticate with invalid credentials', async () => {
        const user = await factory.create('User', {
            password: '123456'
        });


        const response = await request(app)
            .post('/login')
            .send({
                email: user.email,
                password: '12345'
            })

        expect(response.status).toBe(401);
    });

    it('should return jwt token', async () => {
        const user = await factory.create('User', {
            password: '123456'
        });
        //console.log(user);
        const response = await request(app)
            .post('/login')
            .send({
                email: user.email,
                password: '123456'
            })

        expect(response.body).toHaveProperty("token");
    });

    /*it("should create an user", async () => {
        const user = await factory.create('User', {
            password: '123456'
        });

        const response = await request(app)
          .post("/users")
          .send({
            name: user.name,
            email: user.email,
            password: user.password
          });
    
        expect(response.status).toBe(200);
      });*/
});